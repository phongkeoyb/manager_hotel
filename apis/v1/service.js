'use strict';
const express = require('express'),
    router = express.Router();
const ServicePresenter = require('../../presenters/service_presenter');
router.get('/', async function (req, res) {
    try {
        const services = await ServicePresenter.getServices();
        if (services && services.length) {
            return res.status(200).json({ status: 1, message: 'Success', data: services });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});
module.exports = router;