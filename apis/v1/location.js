'use strict';
const express = require('express'),
    router = express.Router();
const LocationPresenter = require('../../presenters/location_presenter');
router.get('/city', async function (req, res) {
    try {
        const code = req.query.code || "";
        const cities = await LocationPresenter.getCities(code);
        if (cities && cities.length) {
            return res.status(200).json({ status: 1, message: 'Success', data: cities });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.get('/ward', async function (req, res) {
    try {
        const code = req.query.code || "";
        if (code) {
            const wards = await LocationPresenter.getWards(code);
            if (wards && wards.length) {
                return res.status(200).json({ status: 1, message: 'Success', data: wards });
            }
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.get('/district', async function (req, res) {
    try {
        const code = req.query.code || "";
        if (code) {
            const districts = await LocationPresenter.getDistricts(code);
            if (districts && districts.length) {
                return res.status(200).json({ status: 1, message: 'Success', data: districts });
            }
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});
module.exports = router;