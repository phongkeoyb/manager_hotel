'use strict';
const express = require('express'),
    router = express.Router();
const RoomPresenter = require('../../presenters/room_presenter');
router.get('/', async function (req, res) {
    try {
        const rooms = await RoomPresenter.getRooms();
        if (rooms && rooms.length) {
            return res.status(200).json({ status: 1, message: 'Success', data: rooms });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});
module.exports = router;