'use strict';
const express = require('express'),
    router = express.Router();
const CustomerPresenter = require('../../presenters/customer_presenter');
router.get('/', async function (req, res) {
    try {
        const q = req.query.q;
        const customers = await CustomerPresenter.getCustomers(q);
        if (customers && customers.length) {
            return res.status(200).json({ status: 1, message: 'Success', data: customers });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.get('/:customer_id', async function (req, res) {
    try {
        const customer_id = req.params.customer_id;
        let customer = await CustomerPresenter.getCustomerById(customer_id)
        if (customer && customer.name) {
            return res.status(200).json({ status: 1, message: 'Success', data: customer });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.post('/', async function (req, res) {
    try {
        let customer = req.body;
        let data = await CustomerPresenter.createCustomers(customer);
        if (data) {
            return res.status(200).json({ status: 1, message: 'Success', data: data });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.put('/:customer_id', async function (req, res) {
    try {
        const customer_id = req.params.customer_id;
        const body = req.body;
        let data = await CustomerPresenter.updateCustomer(customer_id, body)
        if (data) {
            return res.status(200).json({ status: 1, message: 'Success', data: data });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.delete('/:customer_id', async function (req, res) {
    try {
        const customer_id = req.params.customer_id;
        let data = await CustomerPresenter.deleteCustomer(customer_id)
        if (data) {
            return res.status(200).json({ status: 1, message: 'Success', data: data });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

router.get('/export_excel/get', async function (req, res) {
    try {
        let q = req.query.q;
        let file_path = await CustomerPresenter.exportExcelCustomer(q);
        if (file_path) {
            return res.download(file_path);
        }

        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        logger.error(e);
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});

module.exports = router;