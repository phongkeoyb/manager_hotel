'use strict';
const express = require('express'),
    router = express.Router();
const CustomerPresenter = require('../../presenters/customer_presenter');
router.get('/customer', async function (req, res) {
    try {
        let q = req.query.q;
        let file_path = await CustomerPresenter.exportExcelCustomer(q);
        return res.download(file_path);
    } catch (e) {
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});
module.exports = router;