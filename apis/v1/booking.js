'use strict';
const express = require('express'),
    router = express.Router();
const CustomerPresenter = require('../../presenters/customer_presenter');
router.get('/', async function (req, res) {
    try {
        const q = req.query.q;
        const customers = await CustomerPresenter.getCustomers(q);
        if (customers && customers.length) {
            return res.status(200).json({ status: 1, message: 'Success', data: customers });
        }
        return res.status(200).json({ status: 0, message: "Missing parameters" });
    } catch (e) {
        ;
        return res.status(200).json({ status: 0, message: (e.message) ? e.message : 'error' });
    }
});
module.exports = router;