'use strict';

const express = require('express'),
    router = express.Router();
router.use('/customer', require('./customer'));
router.use('/room', require('./room'));
router.use('/service', require('./service'));
router.use('/location', require('./location'));
router.use('/export_excel', require('./export_excel'));
module.exports = router;
