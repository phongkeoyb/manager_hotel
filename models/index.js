'use strict';
const fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose'),
    db = {},
    config = require('config');

mongoose.connect(config.get('mongodb.url'), config.get('mongodb.option'))
    .then(() => {
        console.log("Connect MongoDB Success");

        // import all file in this dir, except index.js
        fs.readdirSync(__dirname)
            .filter(function (file) {
                return (file.indexOf('.') !== 0) && (file !== 'index.js');
            })
            .forEach(function (file) {
                let model = require(path.join(__dirname, file));
                db[model.modelName] = model;
            });
    }).catch((e) => {
        console.log("Connect mongodb Error");
        console.log(e);
    });

db.mongoose = mongoose;
module.exports = db;
