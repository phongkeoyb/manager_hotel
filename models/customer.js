const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Customers = new Schema({
    name: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: true
    },
    is_delete: { // 1: deleted, 0: not delete
        type: Number,
        default: 0
    },
    descriptions: String,
    code_ward: Number,
    name_ward: String,
    code_district: Number,
    name_district: String,
    name_city: String,
    code_city: Number
}, {
    collection: 'customer',
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Customers', Customers);
