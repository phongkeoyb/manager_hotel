const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Districts = new Schema({
    name: {
        type: String,
        require: true
    },
    code: Number,
    code_city: Number,

    is_delete: { // 1: deleted, 0: not delete
        type: Number,
        default: 0
    }
}, {
    collection: 'district',
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Districts', Districts);