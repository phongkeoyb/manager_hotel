const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Booking = new Schema({
    room_code: String,
    list_service: [],
    customer: {},
    money_total: Number,
    checkIn: String,
    checkOut: String,
    descriptions: String
}, {
    collection: 'booking',
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Booking', Booking);
