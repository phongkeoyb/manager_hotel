const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Services = new Schema({
    name: {
        type: String,
        require: true
    },
    code: {
        type: String,
        require: true
    },
    price: Number,
    is_delete: { // 1: deleted, 0: not delete
        type: Number,
        default: 0
    },
    descriptions: String
}, {
    collection: 'service',
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Services', Services);
