const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Rooms = new Schema({
    name: {
        type: String,
        require: true
    },
    code: {
        type: String,
        require: true
    },
    status: {
        type: Number,
        require: 0 //1:null, 2:full
    },
    price: Number,
    is_delete: { // 1: deleted, 0: not delete
        type: Number,
        default: 0
    },
    descriptions: String
}, {
    collection: 'room',
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Rooms', Rooms);
