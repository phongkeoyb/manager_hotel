const config = require("config");
const URL_BASE = config.get("location_product.base_url");
const request = require('request');

class LocationService {
    static async getAllCity() {
        return new Promise(function (resolve, reject) {
            const path = config.get('location_product.city');
            const url = URL_BASE + path;
            request({
                url: url,
                body: {},
                method: "GET",
                json: true,
                headers: {},
                timeout: 100000
            }, function (err, response, body) {
                if (err) {
                    return reject(err);
                }

                if (body.status == 1) {
                    return resolve(body.data || []);
                } else {
                    return resolve([]);
                }
            });
        });
    }

    static async getDistrictById(city_id) {
        return new Promise(function (resolve, reject) {
            const path = config.get('location_product.district');
            const url = `${URL_BASE}${path}${city_id}`;
            request({
                url: url,
                body: {},
                method: "GET",
                json: true,
                headers: {},
                timeout: 10000
            }, function (err, response, body) {
                if (err) {
                    return reject(err);
                }

                if (body.status == 1) {
                    return resolve(body.data || []);
                } else {
                    return resolve([]);
                }
            });
        });
    }

    static async getWardById(district_id) {
        return new Promise(function (resolve, reject) {
            const path = config.get('location_product.ward');
            const url = `${URL_BASE}${path}${district_id}`;
            request({
                url: url,
                body: {},
                method: "GET",
                json: true,
                headers: {},
                timeout: 10000
            }, function (err, response, body) {
                if (err) {
                    return reject(err);
                }

                if (body.status == 1) {
                    return resolve(body.data || []);
                } else {
                    return resolve([]);
                }
            });
        });
    }
}

module.exports = LocationService;
