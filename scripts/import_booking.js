const path = require('path'),
    excel_path = path.join(__dirname, './booking.xlsx');
const readXlsxFile = require('read-excel-file/node');
const BookingRepository = require('../repositories/booking_repository');
class ImportBooking {
    static async import() {
        let sheet = 1;
        let rows = await this.readExcelFile(excel_path, sheet);

        if (rows && rows.length) {
            let list_booking = rows.map(e => {
                let list_service = e[5].split(',');
                let booking = {
                    room_code: e[0],
                    customer: {
                        name: e[1],
                        phone: e[2]
                    },
                    checkIn: e[3],
                    checkOut: e[4],
                    list_service: list_service,
                    money_total: e[6]
                }
                return booking;
            })

            if (list_booking && list_booking.length) {
                for (let i = 1; i < list_booking.length; i++) {
                    console.log(list_booking[i]);
                    await BookingRepository.createBooking(list_booking[i]);
                }
            }
        } else {
            console.log('error');
        }
    }

    static async readExcelFile(file_path, sheet) {
        if (sheet) {
            return await readXlsxFile(file_path, { sheet: sheet });
        }

        return await readXlsxFile(file_path);
    }

    static async run() {
        console.log("===RUN===");
        await this.import();
        console.log("===Done===");
    }

}


setTimeout(() => {
    ImportBooking.run();
}, 2000);