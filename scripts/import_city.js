const LocationRepository = require("../repositories/location_repository");
const LocationService = require("../services/location_service");
class ImportCity {
    static async import() {
        const cities = await LocationService.getAllCity();
        //bac
        const north = cities.north;
        for (let i = 0; i < north.length; i++) {
            let params = {
                code: north[i].id,
                name: north[i].name
            };
            await LocationRepository.createCity(params);
        }

        //trung
        const middle = cities.middle;
        for (let i = 0; i < middle.length; i++) {
            let params = {
                code: middle[i].id,
                name: middle[i].name
            }
            await LocationRepository.createCity(params);
        }

        //nam
        const south = cities.south;
        for (let i = 0; i < south.length; i++) {
            let params = {
                code: south[i].id,
                name: south[i].name
            }
            await LocationRepository.createCity(params);
        }
    };

    static async run() {
        console.log("====START====");
        await this.import();
        console.log("====DONE====");
    };
};

setTimeout(() => {
    ImportCity.run();
}, 2000);


