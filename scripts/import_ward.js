const LocationRepository = require("../repositories/location_repository");
const LocationService = require("../services/location_service");

class ImportWard {
    static async import() {
        const districts = await LocationRepository.getAllDistrict();
        const districts_id = districts.map(e => {
            return e.code;
        });

        console.log(districts_id);

        for (let i = 0; i < districts_id.length; i++) {
            let wards = await LocationService.getWardById(districts_id[i]);
            //console.log(wards);
            if (wards.length) {
                for (let j = 0; j < wards.length; j++) {
                    let params = {
                        code: wards[j].id,
                        name: wards[j].name,
                        code_district: districts_id[i]
                    }
                    await LocationRepository.createWard(params);
                }
            }
        }
    };

    static async run() {
        console.log("====START====");
        await this.import();
        console.log("====DONE====");
    };
};

setTimeout(() => {
    ImportWard.run();
}, 2000);
