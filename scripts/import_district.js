const LocationRepository = require("../repositories/location_repository");
const LocationService = require("../services/location_service");

class ImportDistrict {
    static async import() {
        const cities = await LocationRepository.getAllCity();
        const cities_id = cities.map(e => {
            return e.code;
        });

        for (let i = 0; i < cities_id.length; i++) {
            let districts = await LocationService.getDistrictById(cities_id[i]);
            if (districts.length) {
                for (let j = 0; j < districts.length; j++) {
                    let params = {
                        name: districts[j].name,
                        code: districts[j].id,
                        code_city: cities_id[i]
                    }
                    await LocationRepository.createDistrict(params);
                }
            }
        }
    };

    static async run() {
        console.log("====START====");
        await this.import();
        console.log("====DONE====");
    };
};


setTimeout(() => {
    ImportDistrict.run();
}, 2000);
