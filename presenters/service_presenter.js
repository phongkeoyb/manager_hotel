const ServiceRepository = require("../repositories/service_repository");
let ObjectID = require('mongodb').ObjectID;
class ServicePresenter {
    static async getServices(q = "") {
        try {
            let query = { is_delete: 0 }
            let services = await ServiceRepository.getServices(query);
            return services;
        }
        catch (err) {
            throw err;
        }
    }
}

module.exports = ServicePresenter;
