const RoomRepository = require("../repositories/room_repository");
let ObjectID = require('mongodb').ObjectID;
class RoomPresenter {
    static async getRooms() {
        try {
            let query = { is_delete: 0 }
            let rooms = await RoomRepository.getRooms(query);
            return rooms;
        }
        catch (err) {
            throw err;
        }
    }
}

module.exports = RoomPresenter;
