const CustomerRepository = require("../repositories/customer_repository");
let ObjectID = require('mongodb').ObjectID;
class CustomerPresenter {
    static async getCustomers(q = "") {
        try {
            let query = { is_delete: 0 }
            console.log(q);
            if (q) {
                query["$or"] = [
                    { name: { '$regex': q, $options: 'i' } },
                    { phone: { '$regex': q, $options: 'i' } },
                    { address: { '$regex': q, $options: 'i' } }
                ];
            }
            let customers = await CustomerRepository.getCustomers(query);
            return customers;
        }
        catch (err) {
            throw err;
        }
    }

    static async getCustomerById(customer_id) {
        try {
            let query = { is_delete: 0, _id: customer_id };
            let customer = await CustomerRepository.getCustomerByParams(query);
            if (customer) {
                return customer;
            } else {
                throw new Error("Not found customer");
            }
        } catch (err) {
            throw err;
        }
    }

    static async createCustomers(data) {
        try {
            let customer = await CustomerRepository.getCustomerByParams({ is_delete: 0, phone: data.phone });
            if (customer) {
                throw new Error("This customer already exists");
            }
            await CustomerRepository.createCustomer(data);
            return true;
        } catch (err) {
            throw err;
        }
    }

    static async updateCustomer(customer_id, data) {
        try {
            let query = { is_delete: 0, _id: customer_id };
            let customer = await CustomerRepository.getCustomerByParams(query);
            if (!customer) {
                throw new Error("Not found customer");
            }
            customer.name = data.name;
            customer.phone = data.phone;
            customer.address = data.address;
            customer.descriptions = data.descriptions;
            customer.save();
            return customer;
        } catch (err) {
            throw err;
        }
    }

    static async deleteCustomer(customer_id) {
        try {
            let customer = await CustomerRepository.getCustomerByParams({ is_delete: 0, _id: customer_id });
            if (customer) {
                throw new Error("Not found customer");
            }
            customer.is_delete = 1;
            customer.save();
            return true;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CustomerPresenter;
