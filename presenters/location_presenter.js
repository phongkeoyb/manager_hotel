const LocationRepository = require("../repositories/location_repository");
let ObjectID = require('mongodb').ObjectID;
class LocationPresenter {
    static async getCities(code) {
        try {
            let query = { is_delete: 0 };
            if (code) {
                query.code = code && +code;
            }
            let services = await LocationRepository.getCities(query);
            return services;
        }
        catch (err) {
            throw err;
        }
    }

    static async getWards(code = "") {
        try {
            let query = { is_delete: 0 };
            if (code) {
                query.code_district = code && +code;
            }
            let services = await LocationRepository.getWard(query);
            return services;
        }
        catch (err) {
            throw err;
        }
    }

    static async getDistricts(code = "") {
        try {
            let query = { is_delete: 0 };
            if (code) {
                query.code_city = code && +code;
            }
            let services = await LocationRepository.getDistricts(query);
            return services;
        }
        catch (err) {
            throw err;
        }
    }
}

module.exports = LocationPresenter;
