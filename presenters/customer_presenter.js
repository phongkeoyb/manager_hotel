const CustomerRepository = require("../repositories/customer_repository");
const LocationRepository = require("../repositories/location_repository");
const mongoXlsx = require('mongo-xlsx');
let ObjectID = require('mongodb').ObjectID;
class CustomerPresenter {
    static async getCustomers(q = "") {
        try {
            let query = { is_delete: 0 }
            if (q) {
                query["$or"] = [
                    { name: { '$regex': q, $options: 'i' } },
                    { phone: { '$regex': q, $options: 'i' } },
                    { address: { '$regex': q, $options: 'i' } }
                ];
            }
            let customers = await CustomerRepository.getCustomers(query);
            if (customers && !customers.length) return [];
            return customers;
        }
        catch (err) {
            throw err;
        }
    }

    static async getCustomerById(customer_id) {
        try {
            let query = { is_delete: 0, _id: customer_id };
            let customer = await CustomerRepository.getCustomerByParams(query);
            if (customer) {
                return customer;
            } else {
                throw new Error("Not found customer");
            }
        } catch (err) {
            throw err;
        }
    }

    static async createCustomers(data) {
        try {
            let customer = await CustomerRepository.getCustomerByParams({ is_delete: 0, phone: data.phone });
            if (customer) {
                throw new Error("This customer already exists");
            }

            let query = { is_delete: 0 };
            if (data.city) {
                query.code = data.city;
                let cities = await LocationRepository.getCities(query)
                if (cities && cities.length) {
                    data.name_city = cities[0].name;
                }
            }
            if (data.district) {
                query.code = data.district;
                let districts = await LocationRepository.getDistricts(query)
                if (districts && districts.length) {
                    data.name_district = districts[0].name;
                }
            }
            if (data.ward) {
                query.code = data.ward;
                let wards = await LocationRepository.getWard(query)
                if (wards && wards.length) {
                    data.name_ward = wards[0].name;
                }
            }
            let params = {
                name: data.name,
                phone: data.phone,
                address: data.address,
                descriptions: data.descriptions || "",
                code_city: data.city || "",
                code_district: data.district || "",
                code_ward: data.ward || "",
                name_district: data.name_district || "",
                name_city: data.name_city || "",
                name_ward: data.name_ward || ""
            }
            await CustomerRepository.createCustomer(params);
            return true;
        } catch (err) {
            throw err;
        }
    }

    static async updateCustomer(customer_id, data) {
        try {
            let query = { is_delete: 0, _id: customer_id };
            let customer = await CustomerRepository.getCustomerByParams(query);
            if (!customer) {
                throw new Error("Not found customer");
            }
            customer.name = data.name;
            customer.phone = data.phone;
            customer.address = data.address;
            customer.descriptions = data.descriptions;
            customer.save();
            return customer;
        } catch (err) {
            throw err;
        }
    }

    static async deleteCustomer(customer_id) {
        try {
            let customer = await CustomerRepository.getCustomerByParams({ is_delete: 0, _id: customer_id });
            if (!customer) {
                throw new Error("Not found customer");
            }
            customer.is_delete = 1;
            customer.save();
            return true;
        } catch (err) {
            throw err;
        }
    }

    static async exportExcelCustomer(q = "") {
        try {
            let customers = await this.getCustomers(q);
            if (customers && customers.length) {
                let excel_data = customers.map(element => {
                    let address_customer = element.address || "";
                    if (element.name_ward) {
                        address_customer += ", " + element.name_ward;
                    }
                    if (element.name_district) {
                        address_customer += ", " + element.name_district;
                    }
                    if (element.name_city) {
                        address_customer += ", " + element.name_city;
                    }
                    return {
                        "Tên khách hàng": element.name || "",
                        "Số điện thoại": element.phone || "",
                        "Địa chỉ": address_customer || "",
                        "Ghi chú": element.descriptions || ""
                    }
                })
                let time = Math.round(new Date().getTime() / 1000)
                let fileName = `danhsach_${time}.xlsx`;
                let ex_model = mongoXlsx.buildDynamicModel(excel_data);
                return new Promise((resolve, reject) => {
                    let options = {
                        save: true,
                        sheetName: [],
                        fileName: fileName,
                        path: "./export_files",
                        defaultSheetName: "worksheet"
                    };
                    mongoXlsx.mongoData2Xlsx(excel_data, ex_model, options, function (err, data) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(data.fullPath);
                        }
                    });
                });

            } else return null;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CustomerPresenter;
