const models = require("../models");
class CustomerRepository {
    static async getCustomers(params) {
        if (params) {
            return await models.Customers.find(params);
        }
        return await models.Customers.find();
    }

    static async getCustomerByParams(params) {
        if (params) {
            return await models.Customers.findOne(params);
        }
    }

    static async createCustomer(customer) {
        return await models.Customers.create(customer);
    }

    static async countCustomers(params) {
        if (!params) {
            return await models.Customers.countDocuments();
        }
        return await models.Customers.countDocuments(params);
    }
}
module.exports = CustomerRepository;
