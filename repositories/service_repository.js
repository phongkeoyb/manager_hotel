const models = require("../models");
class ServiceRepository {
    static async getServices(params) {
        if (params) {
            return await models.Services.find(params);
        }
        return await models.Services.find();
    }

}
module.exports = ServiceRepository;
