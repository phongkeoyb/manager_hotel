const models = require("../models");
class BookingRepository {
    static async createBooking(params) {
        if (params) {
            return await models.Booking.create(params);
        }
        return false;
    }

}
module.exports = BookingRepository;
