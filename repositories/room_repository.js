const models = require("../models");
class RoomRepository {
    static async getRooms(params) {
        if (params) {
            return await models.Rooms.find(params);
        }
        return await models.Rooms.find();
    }
}
module.exports = RoomRepository;
