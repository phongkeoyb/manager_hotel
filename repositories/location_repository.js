const models = require("../models");
class LocationRepository {
    static async createCity(params) {
        if (params) {
            return await models.Cities.create(params);
        }
    }

    static async getAllCity() {
        return await models.Cities.find({ is_delete: 0 });
    }

    static async getAllDistrict() {
        return await models.Districts.find({ is_delete: 0 });
    }

    static async getCities(params) {
        if (params) {
            return await models.Cities.find(params);
        }
    }

    static async createDistrict(params) {
        if (params) {
            return await models.Districts.create(params);
        }
    }

    static async createWard(params) {
        if (params) {
            return await models.Wards.create(params);
        }
    }

    static async getDistricts(params) {
        if (params) {
            return await models.Districts.find(params);
        }
    }

    static async getWard(params) {
        if (params) {
            return await models.Wards.find(params);
        }
    }

}
module.exports = LocationRepository;
