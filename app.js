'use strict';

const express = require('express');
const path = require('path');
const config = require('config');
const helmet = require('helmet');
const compress = require('compression');
const timeout = require('connect-timeout');
const app = express();

const bodyParser = require('body-parser');

// gzip compression
app.use(compress());

// App Configuration with Swagger
app.set('port', process.env.PORT || config.get("server.port"));
app.use(helmet.frameguard()); // defaults to sameorigin

app.use(helmet.xssFilter());
app.use(helmet.noSniff());
app.use(helmet.ieNoOpen());
// Hide X-Powered-By
app.use(helmet.hidePoweredBy());

app.disable('x-powered-by');
app.enable("trust proxy");
app.set("trust proxy", true);

app.all('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

app.use(express.static(path.join(__dirname, 'views/dist')));

// body parse
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(timeout(1200000));

// import routers
app.use(require('./apis'));

// start server
let server = app.listen(config.get('server.port'), config.get('server.host'), function () {
    let host = server.address().address;
    let port = server.address().port;
    console.log('Server start at http://%s:%s', host, port);
});
server.setTimeout(1200000);

module.exports = app;
